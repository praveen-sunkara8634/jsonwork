import json
import pydash
import requests
import jsonpath

# data = '{"k1":"val1", "k2":"val2"}'
# jsonResult = json.loads(data)
# print(jsonResult['k1'])

#apiUrl="https://reqres.in/api/users?page=2"
path = "./initial_setup.json"
path2 = "./locators.json"
#resp1 = requests.get(apiUrl)

#validate response status code
#assert resp1.status_code == 200

#parsing response data to json format
# jsonResp = json.loads(resp1.text)
#
# print(resp1.text)
#
# print(jsonResp)
#
# #apply jsonpath
# val = jsonpath.jsonpath(jsonResp,'data[0].first_name')
# print(val[0])
#
# for val in jsonResp['data']:
#     print(val['first_name'])

with open(path) as d:
    data = json.load(d)

# print(data.url)
# key = "url"
# print(f"{data}.{key}")
#
# def get_url(josn_data, key):
#     val = f"{josn_data}.{key}"
#     print(val)
#
# get_url(data, "url")

# print(jsonpath.jsonpath(data, 'url'))
# print(data.get('url'))
key = "authentication_page.username_xpath"
key2 = "url"
print(pydash.get(data, key2))
